var MyObject = {
	add: function (a, b) {
		return a+b;
	},

	subtract: function (a, b) {
		return a-b;
	}
}

module.exports = MyObject;